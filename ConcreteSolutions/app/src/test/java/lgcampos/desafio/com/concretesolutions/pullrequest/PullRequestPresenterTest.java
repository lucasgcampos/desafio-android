package lgcampos.desafio.com.concretesolutions.pullrequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import lgcampos.desafio.com.concretesolutions.shared.models.api.Owner;
import lgcampos.desafio.com.concretesolutions.shared.models.api.PullRequest;
import lgcampos.desafio.com.concretesolutions.shared.models.api.Repository;
import lgcampos.desafio.com.concretesolutions.shared.models.view.PullRequestModelView;
import lgcampos.desafio.com.concretesolutions.shared.services.PullRequestService;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Pull request presenter test
 *
 * @author Lucas Campos
 * @since 1.0.0
 */
@RunWith(MockitoJUnitRunner.class)
public class PullRequestPresenterTest {

    @Mock private Owner owner;
    @Mock private PullRequestView view;
    @Mock private Repository repository;
    @Mock private PullRequestService pullRequestService;

    @Captor private ArgumentCaptor<List<PullRequestModelView>> pullRequestCaptor;

    private InOrder inOrder;
    private PullRequestPresenter presenter;
    private CompositeSubscription compositeSubscription;

    @Before
    public void setUp() {
        inOrder = inOrder(view, pullRequestService);

        when(repository.getOwner()).thenReturn(owner);
        compositeSubscription = new CompositeSubscription();
        presenter = new PullRequestPresenter(view, pullRequestService, repository, compositeSubscription);
    }

    @Test
    public void shouldShowPullRequests() {
        // given
        List<PullRequest> pullRequestsResponse = createPullRequests();
        final long openExpected = 2;
        final long closedExpected = 1;

        when(owner.getLogin()).thenReturn("login");
        when(repository.getName()).thenReturn("name");
        when(pullRequestService.getPullRequests(repository.getOwner().getLogin(), repository.getName())).thenReturn(Observable.just(pullRequestsResponse));

        // when
        presenter.initialize();

        // then
        inOrder.verify(pullRequestService).getPullRequests(repository.getOwner().getLogin(), repository.getName());
        inOrder.verify(view).showProgress();
        inOrder.verify(view).showPullRequests(pullRequestCaptor.capture(), eq(openExpected), eq(closedExpected));
        inOrder.verify(view).dismissProgress();

        List<PullRequestModelView> results = pullRequestCaptor.getValue();
        assertThat(results).hasSize(3);

        verify(view, never()).showNotFoundResults();
        verify(view, never()).showUnavailableNetworkMessage();
        verify(view, never()).showIsNotPossibleCompleteRequestMessage();
    }

    @Test
    public void shouldShowNotFoundResultsWhenResponseIsEmpty() {
        // given
        when(owner.getLogin()).thenReturn("login");
        when(repository.getName()).thenReturn("name");
        when(pullRequestService.getPullRequests(repository.getOwner().getLogin(), repository.getName())).thenReturn(Observable.just(Collections.emptyList()));

        // when
        presenter.initialize();

        // then
        inOrder.verify(view).showProgress();
        inOrder.verify(view).showNotFoundResults();
        inOrder.verify(view).dismissProgress();

        verify(view, never()).showUnavailableNetworkMessage();
        verify(view, never()).showIsNotPossibleCompleteRequestMessage();
        verify(view, never()).showPullRequests(anyList(), anyInt(), anyInt());
    }

    @Test
    public void shouldShowUnavailableNetworkMessage() {
        // given
        when(owner.getLogin()).thenReturn("login");
        when(repository.getName()).thenReturn("name");
        when(pullRequestService.getPullRequests(repository.getOwner().getLogin(), repository.getName())).thenReturn(Observable.error(new UnknownHostException()));

        // when
        presenter.initialize();

        // then
        inOrder.verify(view).showProgress();
        inOrder.verify(view).showUnavailableNetworkMessage();

        verify(view, never()).showIsNotPossibleCompleteRequestMessage();
        verify(view, never()).showPullRequests(anyList(), anyInt(), anyInt());
    }

    @Test
    public void shouldShowItsNotPossibleCompleteRequestMessage() {
        // given
        when(owner.getLogin()).thenReturn("login");
        when(repository.getName()).thenReturn("name");
        when(pullRequestService.getPullRequests(repository.getOwner().getLogin(), repository.getName())).thenReturn(Observable.error(new Throwable()));

        // when
        presenter.initialize();

        // then
        inOrder.verify(view).showProgress();
        inOrder.verify(view).dismissProgress();
        inOrder.verify(view).showIsNotPossibleCompleteRequestMessage();

        verify(view, never()).showNotFoundResults();
        verify(view, never()).showUnavailableNetworkMessage();
        verify(view, never()).showPullRequests(anyList(), anyInt(), anyInt());
    }

    @Test
    public void shouldUnSubscribeCompositionWhenUnSubscribe() {
        // when
        presenter.unSubscribe();

        // then
        assertThat(compositeSubscription.hasSubscriptions()).isFalse();
    }

    private List<PullRequest> createPullRequests() {
        PullRequest pullRequest1 = new PullRequest();
        pullRequest1.setState(PullRequest.State.OPEN.getState());

        PullRequest pullRequest2 = new PullRequest();
        pullRequest2.setState(PullRequest.State.CLOSED.getState());

        PullRequest pullRequest3 = new PullRequest();
        pullRequest3.setState(PullRequest.State.OPEN.getState());

        return Arrays.asList(pullRequest1, pullRequest2, pullRequest3);
    }
}