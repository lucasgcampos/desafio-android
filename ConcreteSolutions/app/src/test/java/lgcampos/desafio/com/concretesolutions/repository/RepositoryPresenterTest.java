package lgcampos.desafio.com.concretesolutions.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collections;

import lgcampos.desafio.com.concretesolutions.shared.models.api.Repository;
import lgcampos.desafio.com.concretesolutions.shared.models.api.SearchRepositoryResponse;
import lgcampos.desafio.com.concretesolutions.shared.services.RepositoryService;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Repository presenter test
 *
 * @author Lucas Campos
 * @since 1.0.0
 */
@RunWith(MockitoJUnitRunner.class)
public class RepositoryPresenterTest {

    @Mock private RepositoryView view;
    @Mock private RepositoryService repositoryService;

    private RepositoryPresenter presenter;
    private CompositeSubscription compositeSubscription;

    @Before
    public void setUp() {
        compositeSubscription = new CompositeSubscription();
        presenter = new RepositoryPresenter(view, repositoryService, compositeSubscription);
    }

    @After
    public void tearDown() {
        compositeSubscription.unsubscribe();
    }

    @Test
    public void shouldGetFavoritesJavaRepositories() {
        // given
        int page = 0;
        SearchRepositoryResponse response = createRepositoryResponse();
        when(repositoryService.getFavoritesJavaRepositories(page)).thenReturn(Observable.just(response));

        // when
        presenter.getFavoritesJavaRepositoriesOfPage(page);

        // then
        InOrder inOrder = inOrder(view);

        inOrder.verify(view).showProgress();
        inOrder.verify(view).dismissProgress();
        inOrder.verify(view).showRepositories(response.getItems());

        verify(view, never()).showNotFoundResults();
        verify(view, never()).showUnavailableNetworkMessage();
        verify(view, never()).showIsNotPossibleCompleteRequestMessage();
    }

    @Test
    public void shouldShowRepositoriesNotFoundWhenReturnNullResponse() {
        // given
        int page = 0;
        when(repositoryService.getFavoritesJavaRepositories(page)).thenReturn(Observable.just(null));

        // when
        presenter.getFavoritesJavaRepositoriesOfPage(page);

        // then
        InOrder inOrder = inOrder(view);

        inOrder.verify(view).showProgress();
        inOrder.verify(view).dismissProgress();
        inOrder.verify(view).showNotFoundResults();

        verify(view, never()).showRepositories(anyList());
        verify(view, never()).showUnavailableNetworkMessage();
        verify(view, never()).showUnavailableNetworkMessage();
        verify(view, never()).showIsNotPossibleCompleteRequestMessage();
    }

    @Test
    public void shouldShowRepositoriesNotFoundWhenReturnNullListOfRepositories() {
        // given
        int page = 0;
        SearchRepositoryResponse nullListOfRepositoryResponse = new SearchRepositoryResponse();
        nullListOfRepositoryResponse.setItems(null);
        when(repositoryService.getFavoritesJavaRepositories(page)).thenReturn(Observable.just(nullListOfRepositoryResponse));

        // when
        presenter.getFavoritesJavaRepositoriesOfPage(page);

        // then
        InOrder inOrder = inOrder(view);

        inOrder.verify(view).showProgress();
        inOrder.verify(view).dismissProgress();
        inOrder.verify(view).showNotFoundResults();

        verify(view, never()).showRepositories(anyList());
        verify(view, never()).showUnavailableNetworkMessage();
        verify(view, never()).showUnavailableNetworkMessage();
        verify(view, never()).showIsNotPossibleCompleteRequestMessage();
    }

    @Test
    public void shouldShowRepositoriesNotFoundWhenReturnEmptyListOfRepositories() {
        // given
        int page = 0;
        SearchRepositoryResponse emptyResponse = new SearchRepositoryResponse();
        emptyResponse.setItems(Collections.emptyList());
        when(repositoryService.getFavoritesJavaRepositories(page)).thenReturn(Observable.just(emptyResponse));

        // when
        presenter.getFavoritesJavaRepositoriesOfPage(page);

        // then
        InOrder inOrder = inOrder(view);

        inOrder.verify(view).showProgress();
        inOrder.verify(view).dismissProgress();
        inOrder.verify(view).showNotFoundResults();

        verify(view, never()).showRepositories(anyList());
        verify(view, never()).showUnavailableNetworkMessage();
        verify(view, never()).showUnavailableNetworkMessage();
        verify(view, never()).showIsNotPossibleCompleteRequestMessage();
    }

    @Test
    public void shouldShowUnavailableNetworkMessage() {
        // given
        int page = 0;
        when(repositoryService.getFavoritesJavaRepositories(page)).thenReturn(Observable.error(new UnknownHostException()));

        // when
        presenter.getFavoritesJavaRepositoriesOfPage(page);

        // then
        InOrder inOrder = inOrder(view);

        inOrder.verify(view).showProgress();
        inOrder.verify(view).showUnavailableNetworkMessage();

        verify(view, never()).showNotFoundResults();
        verify(view, never()).showRepositories(anyList());
        verify(view, never()).showIsNotPossibleCompleteRequestMessage();
    }

    @Test
    public void shouldShowItsNotPossibleCompleteRequestMessageWhenSomethingWrongHappen() {
        // given
        int page = 0;
        when(repositoryService.getFavoritesJavaRepositories(page)).thenReturn(Observable.error(new Throwable()));

        // when
        presenter.getFavoritesJavaRepositoriesOfPage(page);

        // then
        InOrder inOrder = inOrder(view, repositoryService);


        inOrder.verify(repositoryService).getFavoritesJavaRepositories(page);
        inOrder.verify(view).showProgress();
        inOrder.verify(view).dismissProgress();
        inOrder.verify(view).showIsNotPossibleCompleteRequestMessage();

        verify(view, never()).showNotFoundResults();
        verify(view, never()).showRepositories(anyList());
        verify(view, never()).showUnavailableNetworkMessage();
    }

    @Test
    public void shouldUnSubscribeCompositionWhenUnSubscribe() {
        // when
        presenter.unSubscribe();

        // then
        assertThat(compositeSubscription.hasSubscriptions()).isFalse();
    }

    private SearchRepositoryResponse createRepositoryResponse() {
        Repository repository1 = new Repository();
        Repository repository2 = new Repository();
        Repository repository3 = new Repository();

        SearchRepositoryResponse response = new SearchRepositoryResponse();
        response.setItems(Arrays.asList(repository1, repository2, repository3));

        return response;
    }
}