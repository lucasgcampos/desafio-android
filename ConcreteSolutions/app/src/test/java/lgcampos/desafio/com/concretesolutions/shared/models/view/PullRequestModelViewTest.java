package lgcampos.desafio.com.concretesolutions.shared.models.view;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import lgcampos.desafio.com.concretesolutions.shared.models.api.Owner;
import lgcampos.desafio.com.concretesolutions.shared.models.api.PullRequest;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Pull request model view test
 *
 * @author Lucas Campos
 * @since 1.0.0
 */
public class PullRequestModelViewTest {

    private final String login = "LOGIN";
    private final String avatar = "AVATAR";
    private final String html = "HTML";
    private final String title = "TITLE";
    private final String body = "BODY";
    private final Date createAt = new Date();
    private final Owner owner = new Owner(login, avatar);

    @Test
    public void shouldConvertPullRequestToViewCorrectly() {
        // given
        PullRequest pullRequest = createFullPullRequest();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MMM/yy HH:mm", Locale.getDefault());

        // when
        PullRequestModelView result = new PullRequestModelView(pullRequest);

        // then
        assertThat(result.getTitle()).isEqualTo(title);
        assertThat(result.getBody()).isEqualTo(body);
        assertThat(result.getHtmlUrl()).isEqualTo(html);
        assertThat(result.getCreatedAt()).isEqualTo(formatter.format(createAt));

        assertThat(result.getUser().getLogin()).isEqualTo(login);
        assertThat(result.getUser().getAvatarUrl()).isEqualTo(avatar);
    }

    @Test
    public void shouldHaveNullSafeInConversion() {
        PullRequest pullRequest = createEmptyPullRequest();

        // when
        PullRequestModelView result = new PullRequestModelView(pullRequest);

        // then
        assertThat(result.getTitle()).isEqualTo("");
        assertThat(result.getBody()).isEqualTo("");
        assertThat(result.getHtmlUrl()).isEqualTo("");
        assertThat(result.getCreatedAt()).isEqualTo("");

        assertThat(result.getUser().getLogin()).isEqualTo(null);
        assertThat(result.getUser().getAvatarUrl()).isEqualTo(null);
    }

    private PullRequest createEmptyPullRequest() {
        return new PullRequest();
    }

    private PullRequest createFullPullRequest() {
        return new PullRequest(html, body, title, createAt, owner);
    }
}