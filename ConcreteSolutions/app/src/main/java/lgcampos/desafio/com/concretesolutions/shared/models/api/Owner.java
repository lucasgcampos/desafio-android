package lgcampos.desafio.com.concretesolutions.shared.models.api;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Response model of owner of {@link Repository}
 *
 * @author Lucas Campos
 * @since 1.0.0
 */

public class Owner implements Parcelable {

    private String login;

    private String avatar_url;

    public String getLogin() {
        return login;
    }

    public String getAvatarUrl() {
        return avatar_url;
    }

    public Owner(String login, String avatar_url) {
        this.login = login;
        this.avatar_url = avatar_url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.login);
        dest.writeString(this.avatar_url);
    }

    public Owner() {
    }

    protected Owner(Parcel in) {
        this.login = in.readString();
        this.avatar_url = in.readString();
    }

    public static final Creator<Owner> CREATOR = new Creator<Owner>() {
        @Override
        public Owner createFromParcel(Parcel source) {
            return new Owner(source);
        }

        @Override
        public Owner[] newArray(int size) {
            return new Owner[size];
        }
    };
}
