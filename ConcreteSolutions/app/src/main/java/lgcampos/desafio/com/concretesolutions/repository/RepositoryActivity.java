package lgcampos.desafio.com.concretesolutions.repository;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import lgcampos.desafio.com.concretesolutions.R;
import lgcampos.desafio.com.concretesolutions.base.BaseActivity;
import lgcampos.desafio.com.concretesolutions.pullrequest.PullRequestActivity;
import lgcampos.desafio.com.concretesolutions.repository.adapter.RepositoryAdapter;
import lgcampos.desafio.com.concretesolutions.shared.config.retrofit.RetrofitConfig;
import lgcampos.desafio.com.concretesolutions.shared.listeners.EndlessRecyclerViewScrollListener;
import lgcampos.desafio.com.concretesolutions.shared.models.api.Repository;
import lgcampos.desafio.com.concretesolutions.shared.services.RepositoryService;
import rx.subscriptions.CompositeSubscription;

/**
 * Show most famous repositories Java
 *
 * @author Lucas Campos
 * @since 1.0.0
 */
public class RepositoryActivity extends BaseActivity implements RepositoryView, RepositoryAdapter.OnClickRepositoryAdapter {

    public static final String REPOSITORY_DATA = "REPOSITORY_DATA";

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private Unbinder butterKnife;
    private RepositoryAdapter adapter;
    private RepositoryPresenter presenter;
    private final List<Repository> repositories = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repository);
        setUpToolbar();

        butterKnife = ButterKnife.bind(this);

        configureRecyclerView();

        RepositoryService repositoryService = new RetrofitConfig().createService(RepositoryService.class);
        presenter = new RepositoryPresenter(this, repositoryService, new CompositeSubscription());

        if (savedInstanceState != null && savedInstanceState.getParcelableArrayList(REPOSITORY_DATA) != null) {
            List<Repository> results = savedInstanceState.getParcelableArrayList(REPOSITORY_DATA);

            if (results != null && !results.isEmpty()) {
                repositories.addAll(results);
                adapter.notifyDataSetChanged();

                return;
            }
        }

        presenter.initialize();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(REPOSITORY_DATA, new ArrayList<>(repositories));

        super.onSaveInstanceState(outState);
    }

    @Override
    public void showRepositories(List<Repository> items) {
        repositories.addAll(items);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showUnavailableNetworkMessage() {
        snackUnavailableNetworkMessage(recyclerView);
    }

    @Override
    public void showIsNotPossibleCompleteRequestMessage() {
        snackSomethingWrongHappen(recyclerView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        butterKnife.unbind();
        presenter.unSubscribe();
    }

    private void configureRecyclerView() {
        adapter = new RepositoryAdapter(this, repositories);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                presenter.getFavoritesJavaRepositoriesOfPage(page);
            }
        };

        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(scrollListener);
    }

    @Override
    public void onClickRepositoryAdapter(Repository repository) {
        Intent intent = new Intent(this, PullRequestActivity.class);
        intent.putExtra(REPOSITORY_DATA, repository);
        startActivity(intent);
    }
}
