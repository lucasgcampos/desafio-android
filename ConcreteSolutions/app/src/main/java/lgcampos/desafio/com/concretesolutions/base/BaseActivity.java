package lgcampos.desafio.com.concretesolutions.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import lgcampos.desafio.com.concretesolutions.R;
import lgcampos.desafio.com.concretesolutions.repository.RepositoryActivity;

/**
 * Base activity
 *
 * @author Lucas Campos
 * @since 1.0.0
 */
public class BaseActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.progress_dialog_message));
    }

    protected void setUpToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (toolbar != null && RepositoryActivity.class != this.getClass()) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    public void showProgress() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    public void dismissProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public void showNotFoundResults() {
        Toast.makeText(this, R.string.any_result_was_found, Toast.LENGTH_SHORT).show();
    }


    protected void snackUnavailableNetworkMessage(View view) {
        dismissProgress();
        Snackbar.make(view, R.string.no_connection_available_message, Snackbar.LENGTH_LONG).show();
    }

    protected void snackSomethingWrongHappen(View view) {
        Snackbar.make(view, R.string.something_wrong_happen, Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
