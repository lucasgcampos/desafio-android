package lgcampos.desafio.com.concretesolutions.repository.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lgcampos.desafio.com.concretesolutions.R;
import lgcampos.desafio.com.concretesolutions.shared.models.api.Repository;
import lgcampos.desafio.com.concretesolutions.shared.widgets.RoundedImageView;

/**
 * Adapter of {@link Repository}
 *
 * @author Lucas Campos
 * @since 1.0.0
 */

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.ViewHolder> {

    private final List<Repository> repositories;
    private final OnClickRepositoryAdapter onClickAction;

    public RepositoryAdapter(OnClickRepositoryAdapter onClickAction, List<Repository> repositories) {
        this.repositories = repositories;
        this.onClickAction = onClickAction;
    }

    @Override
    public RepositoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_repository, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Repository repository = repositories.get(position);

        holder.userLogin.setText(repository.getOwner().getLogin());
        Picasso.with(holder.itemView.getContext()).load(repository.getOwner().getAvatarUrl()).into(holder.userPhoto);

        holder.repositoryName.setText(repository.getName());
        holder.repositorydescription.setText(repository.getDescription());
        holder.repositoryForks.setText(repository.getForks());
        holder.repositoryStarts.setText(repository.getStargazersCount());

        holder.itemView.setOnClickListener(view -> onClickAction.onClickRepositoryAdapter(repository));
    }

    @Override
    public int getItemCount() {
        return repositories != null ? repositories.size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.repository_name)
        TextView repositoryName;

        @BindView(R.id.repository_description)
        TextView repositorydescription;

        @BindView(R.id.repository_forks)
        TextView repositoryForks;

        @BindView(R.id.repository_stars)
        TextView repositoryStarts;

        @BindView(R.id.user_name)
        TextView userName;

        @BindView(R.id.user_login)
        TextView userLogin;

        @BindView(R.id.user_photo)
        RoundedImageView userPhoto;

        ViewHolder(View view) {
            super(view);

            ButterKnife.bind(this, view);
        }
    }

    public interface OnClickRepositoryAdapter {
        void onClickRepositoryAdapter(Repository repository);
    }

}
