package lgcampos.desafio.com.concretesolutions.shared.config.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static rx.android.schedulers.AndroidSchedulers.mainThread;
import static rx.schedulers.Schedulers.io;

/**
 * {@link Retrofit} configuration
 *
 * @author Lucas Campos
 * @since 1.0.0
 */

public class RetrofitConfig {

    private static final String BASE_URL = "https://api.github.com/";
    private Retrofit retrofit;

    public RetrofitConfig() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxThreadCallAdapter.create(io(), mainThread()))
                .build();
    }

    public <T> T createService(Class<T> tClass) {
        return retrofit.create(tClass);
    }

}
