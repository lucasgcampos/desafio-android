package lgcampos.desafio.com.concretesolutions.shared.models.api;

import java.util.List;

/**
 * Search repository response
 *
 * @author Lucas Campos
 * @since 1.0.0
 */
public class SearchRepositoryResponse {
    List<Repository> items;

    public List<Repository> getItems() {
        return items;
    }

    public void setItems(List<Repository> items) {
        this.items = items;
    }
}
