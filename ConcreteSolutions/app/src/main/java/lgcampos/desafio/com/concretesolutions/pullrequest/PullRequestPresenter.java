package lgcampos.desafio.com.concretesolutions.pullrequest;

import android.util.Log;

import com.annimon.stream.Stream;

import lgcampos.desafio.com.concretesolutions.shared.models.api.PullRequest;
import lgcampos.desafio.com.concretesolutions.shared.models.api.Repository;
import lgcampos.desafio.com.concretesolutions.shared.models.view.PullRequestModelView;
import lgcampos.desafio.com.concretesolutions.shared.services.PullRequestService;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;

import static lgcampos.desafio.com.concretesolutions.shared.transformers.NetworkUnavailableOperator.onNetworkUnavailable;

/**
 * {@link PullRequestActivity}'s presenter
 *
 * @author Lucas Campos
 * @since 1.0.0
 */
class PullRequestPresenter {

    private static final String TAG = PullRequestPresenter.class.getSimpleName();

    private final PullRequestView view;
    private final Repository repository;
    private final PullRequestService pullRequestService;
    private final CompositeSubscription compositeSubscription;

    PullRequestPresenter(PullRequestView view, PullRequestService pullRequestService, Repository repository, CompositeSubscription compositeSubscription) {
        this.view = view;
        this.repository = repository;
        this.pullRequestService = pullRequestService;
        this.compositeSubscription = compositeSubscription;
    }

    void initialize() {
        pullRequestService.getPullRequests(repository.getOwner().getLogin(), repository.getName())
                .doOnSubscribe(view::showProgress)
                .lift(onNetworkUnavailable(view::showUnavailableNetworkMessage))
                .filter(response -> response != null)
                .flatMap(Observable::from)
                .map(PullRequest::toView)
                .toList()
                .doOnError(this::handlerError)
                .subscribe(results -> {
                    if (results.isEmpty()) {
                        view.showNotFoundResults();
                    } else {
                        long open = Stream.of(results).filter(PullRequestModelView::isOpen).count();
                        long close = results.size() - open;

                        view.showPullRequests(results, open, close);
                    }

                    view.dismissProgress();
                }, this::handlerError);
    }

    private void handlerError(Throwable error) {
        Log.e(TAG, error.getMessage(), error);

        view.dismissProgress();
        view.showIsNotPossibleCompleteRequestMessage();
    }

    void unSubscribe() {
        compositeSubscription.unsubscribe();
    }
}
