package lgcampos.desafio.com.concretesolutions.shared.models.view;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.util.Locale;

import lgcampos.desafio.com.concretesolutions.shared.models.api.Owner;
import lgcampos.desafio.com.concretesolutions.shared.models.api.PullRequest;

/**
 * {@link PullRequest} model to display
 *
 * @author Lucas Campos
 * @since 1.0.0
 */
public class PullRequestModelView implements Parcelable {

    private final String state;
    private final String htmlUrl;
    private final String body;
    private final String title;
    private final String createdAt;

    private Owner user;

    public PullRequestModelView(PullRequest pullRequest) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MMM/yy HH:mm", Locale.getDefault());

        this.title = pullRequest.getTitle() != null ? pullRequest.getTitle() : "";
        this.body = pullRequest.getBody() != null ? pullRequest.getBody() : "";
        this.htmlUrl = pullRequest.getHtmlUrl() != null ? pullRequest.getHtmlUrl() : "";
        this.createdAt = pullRequest.getCreatedAt() != null ? formatter.format(pullRequest.getCreatedAt()) : "";
        this.user = pullRequest.getUser() != null ? pullRequest.getUser() : new Owner();
        this.state = pullRequest.getState() != null ? pullRequest.getState() : "";
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public String getBody() {
        return body;
    }

    public String getTitle() {
        return title;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public Owner getUser() {
        return user;
    }

    public boolean isOpen() {
        return PullRequest.State.OPEN.getState().equals(state);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PullRequestModelView that = (PullRequestModelView) o;

        if (htmlUrl != null ? !htmlUrl.equals(that.htmlUrl) : that.htmlUrl != null) return false;
        if (body != null ? !body.equals(that.body) : that.body != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (createdAt != null ? !createdAt.equals(that.createdAt) : that.createdAt != null)
            return false;
        return user != null ? user.equals(that.user) : that.user == null;

    }

    @Override
    public int hashCode() {
        int result = htmlUrl != null ? htmlUrl.hashCode() : 0;
        result = 31 * result + (body != null ? body.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        return result;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.state);
        dest.writeString(this.htmlUrl);
        dest.writeString(this.body);
        dest.writeString(this.title);
        dest.writeString(this.createdAt);
        dest.writeParcelable(this.user, flags);
    }

    protected PullRequestModelView(Parcel in) {
        this.state = in.readString();
        this.htmlUrl = in.readString();
        this.body = in.readString();
        this.title = in.readString();
        this.createdAt = in.readString();
        this.user = in.readParcelable(Owner.class.getClassLoader());
    }

    public static final Parcelable.Creator<PullRequestModelView> CREATOR = new Parcelable.Creator<PullRequestModelView>() {
        @Override
        public PullRequestModelView createFromParcel(Parcel source) {
            return new PullRequestModelView(source);
        }

        @Override
        public PullRequestModelView[] newArray(int size) {
            return new PullRequestModelView[size];
        }
    };
}
