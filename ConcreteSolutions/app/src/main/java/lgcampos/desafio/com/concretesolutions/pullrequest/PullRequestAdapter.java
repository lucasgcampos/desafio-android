package lgcampos.desafio.com.concretesolutions.pullrequest;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lgcampos.desafio.com.concretesolutions.R;
import lgcampos.desafio.com.concretesolutions.shared.models.api.PullRequest;
import lgcampos.desafio.com.concretesolutions.shared.models.view.PullRequestModelView;
import lgcampos.desafio.com.concretesolutions.shared.widgets.RoundedImageView;

/**
 * Adapter of {@link PullRequest}
 *
 * @author Lucas Campos
 * @since 1.0.0
 */
class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.ViewHolder> {

    private final List<PullRequestModelView> pullRequests;
    private final OnClickPullRequestAdapter onClickAction;

    PullRequestAdapter(OnClickPullRequestAdapter onClickAction, List<PullRequestModelView> pullRequests) {
        this.pullRequests = pullRequests;
        this.onClickAction = onClickAction;
    }

    @Override
    public PullRequestAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_pull_request, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PullRequestAdapter.ViewHolder holder, int position) {
        PullRequestModelView pullRequest = pullRequests.get(position);

        holder.pullRequestTitle.setText(pullRequest.getTitle());
        holder.pullRequestBody.setText(pullRequest.getBody());
        holder.pullRequestDate.setText(pullRequest.getCreatedAt());

        holder.userLogin.setText(pullRequest.getUser().getLogin());
        Picasso.with(holder.itemView.getContext()).load(pullRequest.getUser().getAvatarUrl()).into(holder.userPhoto);

        holder.itemView.setOnClickListener(view -> onClickAction.onClickPullRequestAdapter(pullRequest));
    }

    @Override
    public int getItemCount() {
        return pullRequests != null ? pullRequests.size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pull_request_title)
        TextView pullRequestTitle;

        @BindView(R.id.pull_request_body)
        TextView pullRequestBody;

        @BindView(R.id.pull_request_date)
        TextView pullRequestDate;

        @BindView(R.id.user_login)
        TextView userLogin;

        @BindView(R.id.user_photo)
        RoundedImageView userPhoto;


        ViewHolder(View view) {
            super(view);

            ButterKnife.bind(this, view);
        }

    }

    interface OnClickPullRequestAdapter {
        void onClickPullRequestAdapter(PullRequestModelView pullRequest);
    }
}
