package lgcampos.desafio.com.concretesolutions.pullrequest;

import java.util.List;

import lgcampos.desafio.com.concretesolutions.shared.models.api.PullRequest;
import lgcampos.desafio.com.concretesolutions.shared.models.view.PullRequestModelView;

/**
 * {@link PullRequestActivity} view
 *
 * @author Lucas Campos
 * @since 1.0.0
 */
interface PullRequestView {

    void showPullRequests(List<PullRequestModelView> results, long open, long close);

    void showUnavailableNetworkMessage();

    void dismissProgress();

    void showNotFoundResults();

    void showIsNotPossibleCompleteRequestMessage();

    void showProgress();
}
