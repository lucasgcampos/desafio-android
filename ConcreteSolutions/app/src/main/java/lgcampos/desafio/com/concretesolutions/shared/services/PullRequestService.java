package lgcampos.desafio.com.concretesolutions.shared.services;

import java.util.List;

import lgcampos.desafio.com.concretesolutions.shared.models.api.PullRequest;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * {@link PullRequest} service
 *
 * @author Lucas Campos
 * @since 1.0.0
 */
public interface PullRequestService {

    @GET(value = "repos/{owner}/{repository}/pulls?state=all")
    Observable<List<PullRequest>> getPullRequests(@Path("owner") String owner, @Path("repository") String repository);

}
