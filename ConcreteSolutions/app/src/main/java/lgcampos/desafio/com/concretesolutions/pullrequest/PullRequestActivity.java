package lgcampos.desafio.com.concretesolutions.pullrequest;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import lgcampos.desafio.com.concretesolutions.R;
import lgcampos.desafio.com.concretesolutions.base.BaseActivity;
import lgcampos.desafio.com.concretesolutions.repository.RepositoryActivity;
import lgcampos.desafio.com.concretesolutions.shared.config.retrofit.RetrofitConfig;
import lgcampos.desafio.com.concretesolutions.shared.models.api.PullRequest;
import lgcampos.desafio.com.concretesolutions.shared.models.api.Repository;
import lgcampos.desafio.com.concretesolutions.shared.models.view.PullRequestModelView;
import lgcampos.desafio.com.concretesolutions.shared.services.PullRequestService;
import rx.subscriptions.CompositeSubscription;

/**
 * Show {@link PullRequest} of a {@link Repository}
 *
 * @author Lucas Campos
 * @since 1.0.0
 */
public class PullRequestActivity extends BaseActivity implements PullRequestView, PullRequestAdapter.OnClickPullRequestAdapter {

    private static final String PULL_REQUEST_DATA = "PULL_REQUEST_DATA";
    private static final String OPEN_DATA = "OPEN_DATA";
    private static final String CLOSED_DATA = "CLOSED_DATA";

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.amount_opened)
    TextView amountOpened;

    @BindView(R.id.amount_closed)
    TextView amountClosed;

    @BindView(R.id.space)
    TextView separator;

    private Unbinder butterKnife;
    private Repository repository;
    private PullRequestAdapter adapter;
    private PullRequestPresenter presenter;

    private long open;
    private long closed;
    private final List<PullRequestModelView> pullRequests = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);
        setUpToolbar();

        butterKnife = ButterKnife.bind(this);
        configureRecyclerView();

        if (getIntent() != null) {
            repository = getIntent().getParcelableExtra(RepositoryActivity.REPOSITORY_DATA);
            setTitle(repository.getName());
        }

        PullRequestService pullRequestService = new RetrofitConfig().createService(PullRequestService.class);
        presenter = new PullRequestPresenter(this, pullRequestService, repository, new CompositeSubscription());

        if (savedInstanceState != null && savedInstanceState.getParcelableArrayList(PULL_REQUEST_DATA) != null) {
            List<PullRequestModelView> results = savedInstanceState.getParcelableArrayList(PULL_REQUEST_DATA);
            if (results != null && !results.isEmpty()) {
                pullRequests.addAll(results);
                adapter.notifyDataSetChanged();

                this.open = savedInstanceState.getLong(OPEN_DATA);
                this.closed = savedInstanceState.getLong(CLOSED_DATA);

                amountOpened.setText(getString(R.string.amount_pull_request_opened_label, open));
                separator.setText(" / ");
                amountClosed.setText(getString(R.string.amount_pull_request_closed_label, closed));

                return;
            }

        }

        presenter.initialize();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(PULL_REQUEST_DATA, new ArrayList<>(pullRequests));
        outState.putLong(OPEN_DATA, open);
        outState.putLong(CLOSED_DATA, closed);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void showPullRequests(List<PullRequestModelView> results, long open, long close) {
        pullRequests.addAll(results);
        adapter.notifyDataSetChanged();

        this.open = open;
        this.closed = close;

        if (amountOpened != null) {
            amountOpened.setText(getString(R.string.amount_pull_request_opened_label, open));
            separator.setText(" / ");
            amountClosed.setText(getString(R.string.amount_pull_request_closed_label, close));
        }
    }

    @Override
    public void showUnavailableNetworkMessage() {
        snackUnavailableNetworkMessage(recyclerView);
    }

    @Override
    public void showIsNotPossibleCompleteRequestMessage() {
        snackSomethingWrongHappen(recyclerView);
    }

    @Override
    public void onClickPullRequestAdapter(PullRequestModelView pullRequest) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(pullRequest.getHtmlUrl()));
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        butterKnife.unbind();
        presenter.unSubscribe();
    }

    private void configureRecyclerView() {
        adapter = new PullRequestAdapter(this, pullRequests);

        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter.notifyDataSetChanged();
    }

}
