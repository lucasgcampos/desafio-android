package lgcampos.desafio.com.concretesolutions.repository;

import java.util.List;

import lgcampos.desafio.com.concretesolutions.shared.models.api.Repository;

/**
 * {@link RepositoryActivity} view
 *
 * @author Lucas Campos
 * @since 1.0.0
 */
interface RepositoryView {
    void showRepositories(List<Repository> items);

    void showProgress();

    void showUnavailableNetworkMessage();

    void showIsNotPossibleCompleteRequestMessage();

    void showNotFoundResults();

    void dismissProgress();
}
