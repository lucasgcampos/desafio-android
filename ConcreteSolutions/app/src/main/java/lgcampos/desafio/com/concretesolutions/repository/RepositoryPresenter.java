package lgcampos.desafio.com.concretesolutions.repository;

import android.util.Log;

import lgcampos.desafio.com.concretesolutions.shared.services.RepositoryService;
import rx.subscriptions.CompositeSubscription;

import static lgcampos.desafio.com.concretesolutions.shared.transformers.NetworkUnavailableOperator.onNetworkUnavailable;

/**
 * {@link RepositoryActivity}'s presenter
 *
 * @author Lucas Campos
 * @since 1.0.0
 */
class RepositoryPresenter {

    private static final String TAG = RepositoryPresenter.class.getSimpleName();

    private final RepositoryView view;
    private final RepositoryService repositoryService;
    private final CompositeSubscription compositeSubscription;

    RepositoryPresenter(RepositoryView view, RepositoryService repositoryService, CompositeSubscription compositeSubscription) {
        this.view = view;
        this.repositoryService = repositoryService;
        this.compositeSubscription = compositeSubscription;
    }

    void initialize() {
        getFavoritesJavaRepositoriesOfPage(1);
    }

    void getFavoritesJavaRepositoriesOfPage(int page) {
        compositeSubscription.add(repositoryService.getFavoritesJavaRepositories(page)
                .doOnSubscribe(view::showProgress)
                .lift(onNetworkUnavailable(view::showUnavailableNetworkMessage))
                .subscribe(response -> {
                    view.dismissProgress();

                    if (response != null && response.getItems() != null && !response.getItems().isEmpty()) {
                        view.showRepositories(response.getItems());
                    } else {
                        view.showNotFoundResults();
                    }

                }, error -> {
                    Log.e(TAG, error.getMessage(), error);

                    view.dismissProgress();
                    view.showIsNotPossibleCompleteRequestMessage();
                }));
    }

    void unSubscribe() {
        compositeSubscription.unsubscribe();
    }
}
