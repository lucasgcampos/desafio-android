package lgcampos.desafio.com.concretesolutions.shared.models.api;

import java.util.Date;

import lgcampos.desafio.com.concretesolutions.shared.models.view.PullRequestModelView;

/**
 * Model to represent a pull request
 *
 * @author Lucas Campos
 * @since 1.0.0
 */
public class PullRequest {

    private String html_url;
    private String body;
    private String title;
    private String state;
    private Date created_at;

    private Owner user;

    public PullRequest(String html_url, String body, String title, Date created_at, Owner user) {
        this.html_url = html_url;
        this.body = body;
        this.title = title;
        this.created_at = created_at;
        this.user = user;
    }

    public PullRequest() {}

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public Date getCreatedAt() {
        return created_at;
    }

    public Owner getUser() {
        return user;
    }

    public String getHtmlUrl() {
        return html_url;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public PullRequestModelView toView() {
        return new PullRequestModelView(this);
    }

    public enum State {
        OPEN("open"), CLOSED("closed");

        private final String state;

        private State(String state) {
            this.state = state;
        }

        public String getState() {
            return state;
        }
    }

}
