package lgcampos.desafio.com.concretesolutions.shared.models.api;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Model that represent a Github repository
 *
 * @author Lucas Campos
 * @since 1.0.0
 */
public class Repository implements Parcelable {

    private String name;
    private String description;
    private String forks;

    private String stargazers_count;

    private Owner owner;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getForks() {
        return forks;
    }

    public String getStargazersCount() {
        return stargazers_count;
    }

    public Owner getOwner() {
        return owner;
    }




    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.forks);
        dest.writeString(this.stargazers_count);
        dest.writeParcelable(this.owner, flags);
    }

    public Repository() {
    }

    protected Repository(Parcel in) {
        this.name = in.readString();
        this.description = in.readString();
        this.forks = in.readString();
        this.stargazers_count = in.readString();
        this.owner = in.readParcelable(Owner.class.getClassLoader());
    }

    public static final Parcelable.Creator<Repository> CREATOR = new Parcelable.Creator<Repository>() {
        @Override
        public Repository createFromParcel(Parcel source) {
            return new Repository(source);
        }

        @Override
        public Repository[] newArray(int size) {
            return new Repository[size];
        }
    };
}
