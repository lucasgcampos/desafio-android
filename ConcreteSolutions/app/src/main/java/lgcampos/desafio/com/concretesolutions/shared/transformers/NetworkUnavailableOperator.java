package lgcampos.desafio.com.concretesolutions.shared.transformers;

import java.net.UnknownHostException;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action0;

/**
 * Network problem handler
 *
 * @author Lucas Campos
 * @since 1.0.0
 */

public class NetworkUnavailableOperator<T> implements Observable.Operator<T, T> {

    private final Action0 action;

    private NetworkUnavailableOperator(Action0 action) {
        this.action = action;
    }

    public static <T> Observable.Operator<T,T> onNetworkUnavailable(Action0 action){
        return new NetworkUnavailableOperator<>(action);
    }

    @Override
    public Subscriber<? super T> call(Subscriber<? super T> subscriber) {
        return new Subscriber<T>() {
            @Override
            public void onCompleted() {
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onCompleted();
                }
            }

            @Override
            public void onError(Throwable e) {
                if (!subscriber.isUnsubscribed()) {
                    if (e instanceof UnknownHostException) {
                        action.call();
                        subscriber.onCompleted();
                    } else {
                        subscriber.onError(e);
                    }
                }
            }

            @Override
            public void onNext(T t) {
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(t);
                }
            }
        };
    }
}
