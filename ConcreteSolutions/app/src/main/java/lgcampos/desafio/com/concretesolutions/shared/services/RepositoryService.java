package lgcampos.desafio.com.concretesolutions.shared.services;

import lgcampos.desafio.com.concretesolutions.shared.models.api.Repository;
import lgcampos.desafio.com.concretesolutions.shared.models.api.SearchRepositoryResponse;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * {@link Repository} service
 *
 * @author Lucas Campos
 * @since 1.0.0
 */
public interface RepositoryService {

    @GET(value = "search/repositories?q=language:Java&sort=stars")
    Observable<SearchRepositoryResponse> getFavoritesJavaRepositories(@Query("page") int page);

}
